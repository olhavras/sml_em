import numpy as np
import matplotlib.pyplot as plt
np.random.seed(42)
NO_POSES = 4


def print_image(bit_map):
    plt.imshow(bit_map, cmap='Greys', interpolation='nearest')
    plt.show()


def update_etas(etas, shape, average_image):
    beta0 = np.sum(average_image * ~shape) / np.sum(~shape)
    new_eta0 = np.log(beta0 / (1-beta0))
    beta1 = np.sum(average_image * shape) / np.sum(shape)
    new_eta1 = np.log(beta1 / (1-beta1))
    return (new_eta0, new_eta1), ((new_eta0, new_eta1) != etas)


def update_shape(eta0, eta1, old_shape, avg_image):
    eta0_log_prob = avg_image * eta0 - np.log(1 + np.exp(eta0))
    eta1_log_prob = avg_image * eta1 - np.log(1 + np.exp(eta1))
    new_shape = eta0_log_prob < eta1_log_prob
    return new_shape, np.any(new_shape != old_shape)


def shape_mle(average_image, etas, shape=None, print_log=False):
    shape_change_flag, etas_change_flag = True, True
    if shape is None:
        shape = np.random.normal(size=average_image.shape) > 0
    i = 0
    while shape_change_flag or etas_change_flag:
        if print_log:
            print('Iteration {} is processed.'.format(i))
            print('Currently (eta0,eta1)={}.'.format([np.round(eta, 5)for eta in etas]))
        etas, etas_change_flag = update_etas(etas, shape, average_image)
        shape, shape_change_flag = update_shape(*etas, shape, average_image)
        i += 1
        if print_log:
            print_image(shape)
    if print_log:
        print('Process finished.\n')
        plt.title('Final estimate of the shape s (after {} iterations).'.format(i))
        plt.imshow(shape, cmap='Greys', interpolation='nearest')
        plt.savefig('shape_assignment1.png')
    return shape, etas


def assignment1(path):
    arr0 = np.load(path)
    beta0, beta1 = 0.25, 0.75
    etas_init = (np.log(beta0 / (1 - beta0)), np.log(beta1 / (1 - beta1)))
    average_image = np.mean(arr0, axis=0)
    shape, etas = shape_mle(average_image, etas_init, print_log=True)
    print("Parameters after the convergence:\neta0 = {};\neta1 = {}.\n".format(*etas))


def posterior_pose_probs(images, shape, eta0, eta1, poses):
    poses = np.broadcast_to(poses, (len(images), NO_POSES))
    stacked_images = np.stack([images] * NO_POSES, axis=0)
    shapes = np.stack([np.rot90(shape, axes=(0, 1), k=i) for i in range(NO_POSES)], axis=0)
    new_shape = list(shapes.shape)
    new_shape.insert(1, len(images))
    shapes = np.broadcast_to(shapes[:, np.newaxis, :, :], new_shape)
    shapes = np.where(shapes == 0, eta0, eta1)
    softmax_arguments = np.sum(shapes * stacked_images, axis=(2, 3)).T + np.log(poses)
    alphas = np.exp(softmax_arguments) / np.sum(np.exp(softmax_arguments), axis=1)[:, np.newaxis]
    return alphas


def calculate_psi(x, alphas):
    stacked_images = np.stack([np.rot90(x, axes=(1, 2), k=NO_POSES - i) for i in range(NO_POSES)], axis=0)
    alphas = np.broadcast_to(alphas.T[:, :, np.newaxis, np.newaxis], (NO_POSES, x.shape[0], x.shape[1], x.shape[2]))
    psi = np.sum(alphas*stacked_images, axis=(0, 1)) / len(x)
    return psi


def em_algorithm(images, etas, shape, poses):
    i = 1
    alphas = np.random.normal(size=(images.shape[0], NO_POSES)) > 0
    while True:
        print('Starting EM iteration {}.'.format(i))
        #  E-step
        new_alphas = posterior_pose_probs(images, shape, *etas, poses)
        if np.all(new_alphas == alphas):
            print('Stopping criterion reached.\n')
            break
        else:
            alphas = new_alphas
        #  M-step
        poses = np.mean(alphas, axis=0)
        shape, etas = shape_mle(calculate_psi(images, alphas), etas, shape=shape)
        print('New estimates after iteration {}: \netas {};'.format(i, etas))
        print('pose probabilities {}.'.format([np.round(p, 5) for p in poses]))
        i += 1
        print_image(shape)
        print()


    print('EM algorithm stops after {} iterations.'.format(i-1))
    plt.title('Final estimate of the shape s (after {} iterations)'.format(i-1))
    plt.imshow(shape, cmap='Greys', interpolation='nearest')
    plt.savefig('shape_assignment2.png')
    return shape, etas, poses


def assignment2(path):
    arr = np.load(path)
    beta0, beta1 = 0.25, 0.75
    etas_init = (np.log(beta0 / (1 - beta0)), np.log(beta1 / (1 - beta1)))
    shape = np.random.normal(size=arr[0].shape) > 0
    poses = np.array([0.25, 0.25, 0.25, 0.25])
    shape, etas, poses = em_algorithm(arr, etas_init, shape, poses)
    print('Parameters after convergence:\neta0 = {};\neta1 = {}.'.format(*etas))
    print('Estimations of pose prob. are {}'.format(poses))


if __name__ == '__main__':
    print('Assigmnment 1:')
    assignment1('em_data/images0.npy')
    print('Assigmnment 2:')
    assignment2('em_data/images.npy')
